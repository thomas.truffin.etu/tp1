"use strict";

var data = [{
  name: "Regina",
  base: "tomate",
  price_small: 6.5,
  price_large: 9.95,
  image: "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300"
}, {
  name: "Napolitaine",
  base: "tomate",
  price_small: 6.5,
  price_large: 8.95,
  image: "https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300"
}, {
  name: "Spicy",
  base: "crème",
  price_small: 5.5,
  price_large: 8,
  image: "https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300"
}];
var result = data.filter(function (datum) {
  return doubleI(datum);
});
var html = "";
result.forEach(function (datum) {
  html += "<article class=\"pizzaThumbnail\">\n  <a href=\"".concat(datum.image, "\">\n      <img src=\"").concat(datum.image, "\" />\n      <section>\n          <h4>").concat(datum.name, "</h4>\n          <ul>\n              <li> Prix petit format : ").concat(datum.price_small, " \u20AC</li>\n              <li> Prix grand format : ").concat(datum.price_large, " \u20AC</li>\n          </ul>\n      </section>\n  </a>\n</article>");
});
document.querySelector(".pageContent").innerHTML = html;

function alphabetSort(datum1, datum2) {
  if (datum1.name < datum2.name) return -1;else if (datum1.name > datum2.name) return 1;
  return 0;
}

function croissantSort(a, b) {
  return a.price_small - b.price_small;
}

function unCroissantSort(a, b) {
  return b.price_small - a.price_small;
}

function doubleI(a) {
  var test = a.name.toLowerCase().split("i").length - 1;
  console.log(test);
  return a === 2;
}
//# sourceMappingURL=main.js.map