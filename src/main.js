const data = [
  {
    name: "Regina",
    base: "tomate",
    price_small: 6.5,
    price_large: 9.95,
    image:
      "https://images.unsplash.com/photo-1532246420286-127bcd803104?fit=crop&w=500&h=300",
  },
  {
    name: "Napolitaine",
    base: "tomate",
    price_small: 6.5,
    price_large: 8.95,
    image:
      "https://images.unsplash.com/photo-1562707666-0ef112b353e0?&fit=crop&w=500&h=300",
  },
  {
    name: "Spicy",
    base: "crème",
    price_small: 5.5,
    price_large: 8,
    image:
      "https://images.unsplash.com/photo-1458642849426-cfb724f15ef7?fit=crop&w=500&h=300",
  },
];
const result = data.filter(doubleI);
console.log(result);
let html = "";
result.forEach((datum) => {
  html += `<article class="pizzaThumbnail">
  <a href="${datum.image}">
      <img src="${datum.image}" />
      <section>
          <h4>${datum.name}</h4>
          <ul>
              <li> Prix petit format : ${datum.price_small} €</li>
              <li> Prix grand format : ${datum.price_large} €</li>
          </ul>
      </section>
  </a>
</article>`;
});
document.querySelector(".pageContent").innerHTML = html;
function alphabetSort(datum1, datum2) {
  if (datum1.name < datum2.name) return -1;
  else if (datum1.name > datum2.name) return 1;
  return 0;
}
function croissantSort(a, b) {
  return a.price_small - b.price_small;
}
function unCroissantSort(a, b) {
  return b.price_small - a.price_small;
}
function doubleI({ name }) {
  let test = name.toLowerCase().split("i").length - 1;
  return test === 2;
}
